# Android Kotlin


## QUICK LAB
The objective of this application is providing a reservation tool for chemical students from University of Los Andes, so they can accomplish the complex process of laboratories and equipment reservation in an easier way.

## Running the app
The application consists of an Android Studio project in Kotlin language. In order to run the app, you need the Android Studio IDE or IntelliJ IDE. Once it has been imported to the IDE, the app needs a phone emulator to deploy it.  

## Authors
Valerie Parra and Andrea Bayona

## Dependencies

All the dependencies of the repository are in app/build.gradle

## Deploy
### Clone the project: 
Open a folder in your computer and clone the repository

```
git clone https://gitlab.com/isis3510_202020_team31/android-kotlin
```
### Import the project to IDE

Open your favorite IDE and import the app folder 

#### Android Studio
If you use Android Studio go to File->Open and in the dialog, look for cloned repository and import it
<br>
![captura2](/uploads/b0ab59f17e7bda5f43d60ddf9db78283/captura2.PNG)

Sometimes SDK configuration fails. To solve ir clic "set sdk in local.properties and sync project"
<br>

![sdk](/uploads/2d753dccf67e8a82e94807c640415f5c/sdk.PNG)

### Gradle 
The gradle configuration with will run automatically



### Run the code
Look for the "run" button in the IDE of your choice and enjoy the application!
<br>
![Run](/uploads/22068e017c863b700708f0386ec849bc/Captura.PNG)
## License
MIT License

Copyright (c) 2020 ISIS3510_202020_Team3

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


