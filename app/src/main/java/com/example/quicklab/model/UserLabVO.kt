package com.example.quicklab.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
open class UserLabVO (
    val name: String = "",
    val password: String = "",
    val email: String = ""

): Parcelable {
}
