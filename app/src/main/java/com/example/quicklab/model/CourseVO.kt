package com.example.quicklab.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.sql.Timestamp
import java.util.*

@Parcelize
data class CourseVO (
    val id : Int = 0,
    val name: String = "",
    val credits : Int = 0,
    val hoursSpent : Double = 0.0,
    val semester : Timestamp = Timestamp(System.currentTimeMillis()),
    val userLogin : String = ""

): Parcelable {
}
