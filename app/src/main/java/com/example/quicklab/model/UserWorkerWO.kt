package com.example.quicklab.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "user_worker")
@Parcelize
data class UserWorkerWO (
        val name: String = "",
        @PrimaryKey
        val email: String = "",
        val laboratory : String = ""
) : Parcelable {}
