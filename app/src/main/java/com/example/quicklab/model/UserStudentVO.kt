package com.example.quicklab.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "user_student")
@Parcelize
data class UserStudentVO (
    val name: String = "",
    @PrimaryKey
    val email: String = "",
    val code : Int = 0,
    val picture : String = ""

) : Parcelable {}
