package com.example.quicklab.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "equipment")
data class EquipmentVO (
    @PrimaryKey()
    val idE : Int = 0,
    val applications: String = "",
    val cost: Double = 0.0,
    val costType: String = "",
    val description: String = "",
    val equipmentType: String = "",
    val name: String = "",
    val qrVisits: Int = 0,
    val totalVisits: Int = 0,
    val image: String = "") : Parcelable {
}
