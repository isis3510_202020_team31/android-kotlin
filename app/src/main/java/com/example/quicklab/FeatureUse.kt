package com.example.quicklab

import com.google.firebase.Timestamp

class FeatureUse {

    var feature: String
    var date: Timestamp


    constructor(feature: String) {
        this.feature = feature
        this.date = Timestamp.now()
    }
}