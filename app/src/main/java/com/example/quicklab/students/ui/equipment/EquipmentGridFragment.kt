package com.example.quicklab.students.ui.equipment

import android.app.Application
import android.content.ContentValues.TAG
import android.os.Bundle
import android.provider.Contacts
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.asLiveData
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.quicklab.R
import com.example.quicklab.Room.EquipmentRepository
import com.example.quicklab.application.QuickLabApplication
import com.example.quicklab.connectivity.NetworkMonitor
import com.example.quicklab.model.EquipmentVO
import com.google.android.gms.tasks.Tasks.await
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.ktx.Firebase
import com.squareup.okhttp.Dispatcher
import kotlinx.android.synthetic.main.shr_product_grid_fragment.view.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.tasks.await

class EquipmentGridFragment : Fragment() {

    private val repository: EquipmentRepository = QuickLabApplication.repository
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    val APP_SCREEN_CLASS = "EquipmentGridFragment"
    val APP_SCREEN_NAME = "EquipmentList"
    lateinit var productList: MutableList<EquipmentVO>
    var sqlList: List<EquipmentVO> = emptyList()

    private val job: Job = Job()
    //or Dispatchers.IO


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        productList = mutableListOf<EquipmentVO>()

        Log.d("ON CREATE", "se creo equip fragment")
        //Log.d("INSTANCE DB", repository.toString())

        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment with the ProductGrid theme
        var view = inflater.inflate(R.layout.shr_product_grid_fragment, container, false)
        // Set up the RecyclerView
        val adapter = ObjectCardRecyclerViewAdapter(productList)

        viewSettings(view, adapter)
        val internet = NetworkMonitor.isOnline(context!!)

        //No hay internet y los datos no estan en memoria
        if (!internet && productList.size == 0) {
                //Log.d("SIZE EQUIPMENTS DB", sqlList.size.toString())

                    //los datos si estan en disco
                    //Se traen de SQLite
                    if (sqlList.isNotEmpty() and false) {
                        lifecycleScope.launchWhenResumed {

                            showIOData(adapter)
                        }


                     }

                    //los datos no estan en disco
                    else {
                            view = inflater.inflate(R.layout.no_internet, container, false)
                            val message = "Please, connect to Internet to download the equipment information"
                            view.findViewById<TextView>(R.id.textView_noInternet).text = message
                        }


        }

        //Hay internet y los datos no estan en memoria
        //Se traen de internet
        else if (internet && productList.size == 0) {

            lifecycleScope.launch {
                val qS = getDataFromFireStore()
                if (qS != null) {
                    onResult(qS, productList, adapter)
                }
            }
        }


        return view;
    }


    private fun viewSettings(
        view: View,
        adapter: ObjectCardRecyclerViewAdapter
    ) {
        view.recycler_view.setHasFixedSize(true)
        view.recycler_view.layoutManager = GridLayoutManager(context, 2, RecyclerView.VERTICAL, false)
        view.recycler_view.adapter = adapter
        val largePadding = resources.getDimensionPixelSize(R.dimen.shr_product_grid_spacing)
        val smallPadding = resources.getDimensionPixelSize(R.dimen.shr_product_grid_spacing_small)
        view.recycler_view.addItemDecoration(EquipmentGridItemDecoration(largePadding, smallPadding))
    }




    suspend fun getDataFromFireStore(): QuerySnapshot? = withContext(Dispatchers.IO) {
        return@withContext try {
            val data = FirebaseFirestore.getInstance()
                .collection("equipment").orderBy("totalVisits", Query.Direction.DESCENDING)
                .get()
                .await()
            data
        } catch (e: Exception) {
            null
        }
    }


    fun giveListFromQuery(
        qS: QuerySnapshot,
        productList: MutableList<EquipmentVO>
    ): List<EquipmentVO> {
        //var productList: MutableList<Equipment> = mutableListOf<Equipment>()
        val docs = qS.documents
        for (doc in docs) {
            doc.toObject(EquipmentVO::class.java)?.let {
                productList.add(it)
                Log.d(TAG, "entro : " + it.name)
            }
        }
        return productList
    }

    fun onResult(
        qS: QuerySnapshot,
        productList: MutableList<EquipmentVO>,
        adapter: ObjectCardRecyclerViewAdapter
    ) {
        giveListFromQuery(qS, productList)
        adapter.notifyDataSetChanged()
        lifecycleScope.launch {
            insertSQLiteData(productList)
        }
    }

    fun onResult2() {

    }


    private suspend fun getEquipments() =
        withContext(Dispatchers.IO) {

            val defer = async { sqlList = repository.getAll() }
            defer.await()
            return@withContext sqlList

        }


    suspend fun showIOData(adapter: ObjectCardRecyclerViewAdapter) {
        val data = withContext(Dispatchers.IO) {
            return@withContext repository.getAll()
        }
        withContext(Dispatchers.Main) {
            Log.d("EQUIPMENT LIST VIEW ", data.toString())
            productList = data as MutableList<EquipmentVO>
            Log.d("EQUIPMENT LIST VIEW ", productList.toString())
            adapter.notifyDataSetChanged()

        }
    }



    fun insertSQLiteData(list: MutableList<EquipmentVO>) {
        lifecycleScope.launch {
            val insertE = async { repository.insertAll(list) }
            insertE.await()
        }

    }


    override fun onResume() {
        super.onResume()

        // After enough time has passed to make this screen view significant.
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, APP_SCREEN_NAME)
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, APP_SCREEN_CLASS)
        //bundle.putString(MyAppAnalyticsConstants.Param.TOPIC, topic)
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle)
        Log.d(TAG, "Screen Equipos : ")

    }

}



