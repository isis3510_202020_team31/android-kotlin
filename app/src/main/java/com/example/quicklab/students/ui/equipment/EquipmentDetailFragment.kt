package com.example.quicklab.students.ui.equipment

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.quicklab.R
import com.example.quicklab.connectivity.NetworkMonitor
import com.example.quicklab.model.EquipmentVO
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import java.text.DecimalFormat


class EquipmentDetailFragment : Fragment(){

    val args : EquipmentDetailFragmentArgs by navArgs()
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    val APP_SCREEN_CLASS = "EquipmentDetailFragment"
    val APP_SCREEN_NAME = "EquipmentDetail"
    var equip : EquipmentVO? = null
    var nameEquip : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        firebaseAnalytics = Firebase.analytics

    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.shr_equipment_detail, container, false)
        equip = args.EquipmentVO
        if(!NetworkMonitor.isOnline(requireContext()) && equip == null ){
            view = inflater.inflate(R.layout.no_internet, container, false)
            val message = "Please, connect to Internet to download the equipment information"
            view.findViewById<TextView>(R.id.textView_noInternet).text = message

        }

        else {
            //var equipmentSingleton : EquipmentSingleton = EquipmentSingleton.getInstance()

            var imageV =view.findViewById<ImageView>(R.id.image)
            nameEquip = equip!!.name
            //var image: Unit = ImageRequester.setImageFromUrl(a,equipmentSingleton.equipment!!.image)
            view.findViewById<TextView>(R.id.product_title).text = equip!!.name
            view.findViewById<TextView>(R.id.textView7).text = equip!!.description

            val formatter : DecimalFormat = DecimalFormat("#,###,###")
            val numberPrice = formatter.format(equip!!.cost)

            view.findViewById<TextView>(R.id.product_price).text = "$ "+numberPrice.toString() + "/"+ equip!!.costType


            Picasso.get()
                .load(equip!!.image)
                .into(imageV)
        }


        return view
    }


    override fun onResume() {
        super.onResume()

        if(nameEquip != null){
            // After enough time has passed to make this screen view significant.
            val bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, APP_SCREEN_NAME)
            bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, APP_SCREEN_CLASS)
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, nameEquip)
            //bundle.putString(MyAppAnalyticsConstants.Param.TOPIC, topic)
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle)
        }


    }


}
