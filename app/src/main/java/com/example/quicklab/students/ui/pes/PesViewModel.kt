package com.example.quicklab.students.ui.pes

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PesViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {

        value = "PES Format"
    }

    private val _q1 = MutableLiveData<String>().apply {
        value = "Do you belong to the Chemical Engineering Deparment?"
    }

    private val _q2 = MutableLiveData<String>().apply {
        value = "Course/Project"
    }

    val text: LiveData<String> = _text
    val q1: LiveData<String> = _q1

}