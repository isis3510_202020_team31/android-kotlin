package com.example.quicklab.students.ui.pes

class User {
    lateinit var name: String
    lateinit var password: String
    lateinit var email: String
    lateinit var code: Number
    lateinit var rol: String

    constructor(name: String, password: String, email: String, code: Number, rol: String) {
        this.name = name
        this.password = password
        this.email = email
        this.code = code
        this.rol = rol
    }
    constructor(){

    }
}