package com.example.quicklab.students.ui.budget

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.quicklab.R
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase


class BudgetFragment : Fragment() {

    private lateinit var budgetViewModel: BudgetViewModel
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {



        budgetViewModel =
                ViewModelProviders.of(this).get(BudgetViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_budget, container, false)
        val textView: TextView = root.findViewById(R.id.text_home)
        budgetViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }


    override fun onResume() {
        super.onResume()

        // After enough time has passed to make this screen view significant.
        val bundle = Bundle()
//        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "screenName")
//        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, "BudgetFragment")
//        //bundle.putString(MyAppAnalyticsConstants.Param.TOPIC, topic)
//        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle)
    }
}

