package com.example.quicklab.students.ui.homeApp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import com.example.quicklab.R
import com.example.quicklab.Room.UserRepository

class HomeAppFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment with the ProductGrid theme
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        Log.d("ROL", UserRepository.rol)
        if(UserRepository.rol == UserRepository.STUDENT){
            var username = UserRepository.user.name.split(" ")
            view.findViewById<TextView>(R.id.homeUsername).text = username[0]
            view.findViewById<TextView>(R.id.homeUserRol).text = UserRepository.rol + "-" + UserRepository.user.code.toString()
        }
        else{
            var username = UserRepository.worker.name.split(" ")
            view.findViewById<TextView>(R.id.homeUsername).text = username[0]
            view.findViewById<TextView>(R.id.homeUserRol).text = UserRepository.rol
            view.findViewById<CardView>(R.id.transferLimitAmountCardView2).visibility = View.GONE

            val title = "PES forms"
            val content = "You have 0 PES forms to review"

            view.findViewById<TextView>(R.id.homeNONrobustBudgetTitle).text = title
            view.findViewById<TextView>(R.id.homeNonRobustAmount).text = content
        }


        return view
    }


}
