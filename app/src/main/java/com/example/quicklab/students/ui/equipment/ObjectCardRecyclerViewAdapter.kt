package com.example.quicklab.students.ui.equipment

import android.content.ContentValues
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.quicklab.R
import com.example.quicklab.model.EquipmentVO
import com.example.quicklab.students.ui.qrcode.QrFragmentDirections
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import java.text.DecimalFormat


/**
 * Tomado y modificado de https://codelabs.developers.google.com/codelabs/mdc-103-kotlin/#0
 * Adapter used to show a simple grid of products.
 */
class ObjectCardRecyclerViewAdapter internal constructor(private val productList: List<EquipmentVO>) : RecyclerView.Adapter<EquipmentCardViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EquipmentCardViewHolder {
        val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.shr_product_card, parent, false)
        return EquipmentCardViewHolder(layoutView)
    }

    override fun onBindViewHolder(holder: EquipmentCardViewHolder, position: Int) {
        if (position < productList.size) {
            val product = productList[position]
//            Log.d(ContentValues.TAG, "SIZE: " + product.image)
            holder.productTitle.text = product.name

            val formatter : DecimalFormat = DecimalFormat("#,###,###")
            val numberPrice = formatter.format(product.cost)
            holder.productPrice.text = "$ "+numberPrice.toString()
            //ImageRequester.setImageFromUrl(holder.productImage, product.image)
            Picasso.get()
                .load(product.image)
                .into(holder.productImage)

            holder.itemView.setOnClickListener {
                val action = EquipmentGridFragmentDirections.actionNavEquipListToEquipmentDetailFragment(product)
                holder.itemView.findNavController().navigate(action)
            }
        }
    }

    override fun getItemCount(): Int {
        return productList.size
    }
}
