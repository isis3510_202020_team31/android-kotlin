package com.example.quicklab.students.ui.qrcode

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.example.quicklab.R
import com.example.quicklab.connectivity.NetworkMonitor
import com.example.quicklab.model.EquipmentVO
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.ktx.Firebase

import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.fragment_qr_code.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext


class QrFragment : Fragment() {


    private lateinit var firebaseAnalytics: FirebaseAnalytics
    val APP_SCREEN_CLASS = "QrFragment"
    val APP_SCREEN_NAME = "QR scanner"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }



    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment with the ProductGrid theme
        val view = inflater.inflate(R.layout.fragment_qr_code, container, false)

        view.qr_button.setOnClickListener {
            scanQRCode(view, container, inflater)
        }

        firebaseAnalytics = Firebase.analytics

        return view
    }

    private fun scanQRCode(
        view: View,
        container: ViewGroup?,
        inflater: LayoutInflater
    ) {

        var integrator = IntentIntegrator.forSupportFragment(this@QrFragment)

        integrator.setOrientationLocked(true);
        //integrator.setPrompt("Scan QR code");
        integrator.setBeepEnabled(true);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);


        integrator.initiateScan();
    }

    // Get the results:
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        val appContext = requireContext().applicationContext
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) Toast.makeText(appContext, "Cancelled", Toast.LENGTH_LONG).show()
            else {Toast.makeText(appContext, "Scanned: " + result.contents, Toast.LENGTH_LONG).show()
                Log.d(ContentValues.TAG, "entro : " + result.contents.toString())

                lifecycleScope.launch{

                    val internet = NetworkMonitor.isOnline(context!!)


                        val qS = getDataFromFireStore(result.contents.toString())
                        if (qS != null) {
                            val equip = giveObjectFromQuery(qS)
                            Log.d(ContentValues.TAG, "encontro : " + equip.toString())
                            //var equipment_singleton : EquipmentSingleton = EquipmentSingleton.getInstance()
                            //equipment_singleton.equipment = equip
                            val action = QrFragmentDirections.actionNavQrCodeToEquipmentDetailFragment(equip)
                            //view?.findNavController()?.navigate(R.id.action_nav_qr_code_to_equipmentDetailFragment)
                            view?.findNavController()?.navigate(action)
                        }
//                    }
//                    else{
//                       val duration = Toast.LENGTH_LONG
//                        val toast3 = Toast.makeText(context!!, "The functionalities without internet connection are limited", duration)
//                        toast3.show()
//                   }

                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }


    suspend fun getDataFromFireStore(value : String) : QuerySnapshot? = withContext(Dispatchers.IO) {
        return@withContext try {
            val data = FirebaseFirestore.getInstance()
                .collection("equipment")
                .whereEqualTo("name", value)
                .get()
                .await()
            data
        } catch (e: Exception) {
            null
        }
    }


    fun giveObjectFromQuery(qS : QuerySnapshot ): EquipmentVO {
        var productList: MutableList<EquipmentVO> = mutableListOf<EquipmentVO>()
        val docs = qS.documents
        for (doc in docs) {
            doc.toObject(EquipmentVO::class.java)?.let {
                productList.add(it)
                Log.d(ContentValues.TAG, "entro : " + it.name)
            }
        }
        return productList.get(0)
    }

    override fun onResume() {
        super.onResume()

        // After enough time has passed to make this screen view significant.
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, APP_SCREEN_NAME)
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, APP_SCREEN_CLASS)
        //bundle.putString(MyAppAnalyticsConstants.Param.TOPIC, topic)
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle)
        Log.d(ContentValues.TAG, "Screen QR code : ")

    }

}
