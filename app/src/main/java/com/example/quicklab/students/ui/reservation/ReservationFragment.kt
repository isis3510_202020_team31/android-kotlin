package com.example.quicklab.students.ui.reservation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.quicklab.R

class ReservationFragment : Fragment() {

    private lateinit var reservationViewModel: ReservationViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        reservationViewModel =
                ViewModelProviders.of(this).get(ReservationViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_reservation, container, false)
        val textView: TextView = root.findViewById(R.id.text_slideshow)
        reservationViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }
}