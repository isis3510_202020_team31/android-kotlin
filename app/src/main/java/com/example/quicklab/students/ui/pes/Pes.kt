package com.example.quicklab.students.ui.pes

import android.os.Parcelable
import com.google.firebase.Timestamp
import kotlinx.android.parcel.Parcelize
@Parcelize
class Pes (
    var memberChemicalDepartment: Boolean = false,
    var professor: String ="",
    var course: String ="",
    var experiment: String="",
    var laboratoryEquipment: String="",
    var laboratoryMaterial: String="",
    var userName: String?="",
    var laboratory: String="",
    var date: Timestamp = Timestamp.now(),
    var status : String="Waiting")  : Parcelable {

}



