package com.example.quicklab.students.ui.properties

class AntoineConstants{
    var name: String
    var a: Double
    var b: Double
    var c: Double
    constructor(a: Double, b: Double, c: Double, name: String) {
        this.a = a
        this.b = b
        this.c = c
        this.name = name
    }
}