package com.example.quicklab.students.ui.properties


import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlin.math.pow


class PropertiesCalculator {


    var antoine_constants: HashMap<String, AntoineConstants>
    var cp_constants: HashMap<String, CpConstants>
    val T0: Double = 273.15
    val R: Double = 8.314

    constructor(json_cp : String, json_antoine: String){
        val gson = Gson()
        val itemType = object : TypeToken<List<CpConstants>>() {}.type
        val list : List<CpConstants>? = gson.fromJson<List<CpConstants>>(json_cp, itemType)
        cp_constants = HashMap()

        if (list != null) {
            for (i in 0 until list.size){
                cp_constants.put(list.get(i).name, list.get(i))
            }
        }

        val itemType_2 = object : TypeToken<List<AntoineConstants>>() {}.type
        val list_2 : List<AntoineConstants>? = gson.fromJson<List<AntoineConstants>>(json_antoine, itemType_2)
        antoine_constants = HashMap()

        if (list_2 != null) {
            for (i in 0 until list_2.size){
                antoine_constants.put(list_2.get(i).name, list_2.get(i))
            }
        }
    }

    fun calculateCp(substance: String, temperature: Double): Number?{
        val cp_c: CpConstants? = cp_constants.get(substance)
        if(cp_c!=null) {
            val tao = temperature / T0
            var cp: Number = (cp_c.a + cp_c.b / 2 * T0 * (tao + 1) )+ cp_c.c / 3 * T0.pow(3) * (tao.pow(2) + tao + 1) + (cp_c.d / (tao * T0.pow(2)))*(temperature-T0)*R
            return cp.toInt()
        }
        return 0
    }

    fun calculatePsat(substance: String, temperature: Double): Number?{
        val antoine: AntoineConstants? = antoine_constants.get(substance)
        if(antoine!=null){
            var exp_t: Double = antoine.a-(antoine.b/(temperature-273.15+antoine.c))
            var ten : Double = 10.0
            var psat = ten.pow(exp_t)

            return psat.toInt()
        }
        return 0
    }





}
