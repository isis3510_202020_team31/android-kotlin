package com.example.quicklab.students.ui.pes
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.quicklab.R
import com.example.quicklab.UserSingleton
import com.example.quicklab.connectivity.PesRepository
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase


class PesFragment : Fragment(){


    private lateinit var q1Rb1: RadioButton
    private lateinit var q1_rb2: RadioButton
    private lateinit var q2_tv_course: EditText
    private lateinit var q3_tv_professor: EditText
    private lateinit var experiment: EditText
    private lateinit var laboratoryEquipment: EditText
    private lateinit var laboratoryMaterial: EditText
    private lateinit var laboratories: Spinner



    private lateinit var firebaseAnalytics: FirebaseAnalytics
    val APP_SCREEN_CLASS = "PesFragment"
    val APP_SCREEN_NAME = "PesForm"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_pes, container, false)
        val senPes: Button = root.findViewById(R.id.button)
        intializeFormAttributes(root)
        getLastPESFromCache()
        var options = arrayOf("ML-037","ML-206","ML-305","ML-414/416","ML-418")

        laboratories.adapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_list_item_1, options)

        senPes.setOnClickListener @Suppress("UNUSED_ANONYMOUS_PARAMETER") { view: View ->
            var memberChemicalDepartment: Boolean = false;
              if(q1Rb1.isChecked){
                memberChemicalDepartment = true
            }
            var professor: String = this.q3_tv_professor.text.toString()
            var course: String = this.q2_tv_course.text.toString()
            var experimentP: String = this.experiment.getText().toString()
            var laboratoryEquipmentP: String = this.laboratoryEquipment.getText().toString()
            var laboratoryMaterialP: String = this.laboratoryMaterial.getText().toString()
            val db = FirebaseFirestore.getInstance()
            var pes: Pes = Pes(memberChemicalDepartment,
                    professor,
                    course,
                    experimentP,
                    laboratoryEquipmentP,
                    laboratoryMaterialP,
                    FirebaseAuth.getInstance().currentUser?.email,
                    laboratories.selectedItem.toString())
            PesRepository.sendPes(context!!, activity!!, pes, ::callback)
        }
        return root
    }

    private fun callback(response: Boolean){
        if(response){
            hideKeyboard()
            view?.findNavController()?.navigate(R.id.action_nav_pes_to_successPES)
        }
        else{
            hideKeyboard()
            val duration = Toast.LENGTH_LONG
            val toast3 = Toast.makeText(activity, "There was a problem uploading your request to the database. Please try again later.", duration)
            toast3.show()
        }
    }

    private fun hideKeyboard(){
        val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val f = requireActivity().currentFocus
        if (null != f && null != f.windowToken && EditText::class.java.isAssignableFrom(f.javaClass)) {
            imm.hideSoftInputFromWindow(f.windowToken, 0)
        }
        else{
            requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        }
    }

    private fun intializeFormAttributes(root: View){
        q1Rb1 = root.findViewById(R.id.q1_rb1)
        q2_tv_course = root.findViewById(R.id.q2_tv_course)
        q3_tv_professor = root.findViewById(R.id.q3_tv_professor)
        experiment = root.findViewById(R.id.experiment)
        laboratories = root.findViewById(R.id.spinner_lab)
        q1_rb2 = root.findViewById(R.id.q1_rb2)
        laboratoryEquipment = root.findViewById(R.id.laboratory)
        laboratoryMaterial = root.findViewById(R.id.laboratoryMaterial)
    }

    private fun getLastPESFromCache(){
        val preferences: SharedPreferences = requireContext().getSharedPreferences("pes", Context.MODE_PRIVATE)
        val pref = preferences.getBoolean("pes", false)

        if( pref == true ){
            val experimentText = preferences.getString("experiment","noExperiment")
            experiment.setText(experimentText)
            val ml037Boolean = preferences.getBoolean("ml037", false)

            val memberChemicalDepartmentBoolean = preferences.getBoolean("memberChemicalDepartment", false)
            if(memberChemicalDepartmentBoolean){
                q1Rb1.isSelected = true
                q1Rb1.isChecked = true
            }
            else{
                q1_rb2.isChecked = true
                q1_rb2.isSelected = true
            }
            val professorString = preferences.getString("professor", "")
            q3_tv_professor.setText(professorString)
            val courseString = preferences.getString("course", "")
            q2_tv_course.setText(courseString)
            val laboratoryEquipmentString = preferences.getString("laboratoryEquipment", "")
            laboratoryEquipment.setText(laboratoryEquipmentString)
            val laboratoryMaterialString = preferences.getString("laboratoryMaterial", "")
            laboratoryMaterial.setText(laboratoryMaterialString)
        }
    }

    override fun onResume() {
        super.onResume()

        // After enough time has passed to make this screen view significant.
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, APP_SCREEN_NAME)
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, APP_SCREEN_CLASS)
        //bundle.putString(MyAppAnalyticsConstants.Param.TOPIC, topic)
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle)
        Log.d(ContentValues.TAG, "Screen PES : ...")

        if(!PesRepository.isOnline(requireContext())){
            val duration = Toast.LENGTH_LONG
            val toast3 = Toast.makeText(activity, "No internet connection.", duration)
            toast3.show()
        }

    }
}
