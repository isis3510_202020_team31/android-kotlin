package com.example.quicklab.students.ui.userProfile

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.quicklab.R
import com.example.quicklab.Room.UserRepository
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase

class StudentProfileFragment : Fragment() {

    private lateinit var firebaseAnalytics: FirebaseAnalytics
    val APP_SCREEN_CLASS = "StudentProfileFragment"
    val APP_SCREEN_NAME = "StudentProfile"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        firebaseAnalytics = Firebase.analytics
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment with the ProductGrid theme
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        view.findViewById<TextView>(R.id.profile_username).text = UserRepository.user.name
        view.findViewById<TextView>(R.id.profile_email).text = UserRepository.user.email

        return view
    }

    override fun onResume() {
        super.onResume()

        // After enough time has passed to make this screen view significant.
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, APP_SCREEN_NAME)
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, APP_SCREEN_CLASS)
        //bundle.putString(MyAppAnalyticsConstants.Param.TOPIC, topic)
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle)
        Log.d(ContentValues.TAG, "Screen student profile : ")

    }


}
