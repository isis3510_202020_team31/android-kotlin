package com.example.quicklab.students.ui.pes

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.quicklab.R
import com.example.quicklab.UserSingleton
import com.example.quicklab.worker.ui.PesDetailFragmentArgs
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase

class DetailPesStudentFragment : Fragment() {
    val args: PesDetailFragmentArgs by navArgs()
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    val APP_SCREEN_CLASS = "DetailPesStudentFragment"
    val APP_SCREEN_NAME = "PesDetail"
    lateinit var nameEquip: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_detail_pes_student, container, false)
        var equip = args.Pes
        nameEquip = equip.userName.toString()
        view.findViewById<TextView>(R.id.laboratoryMaterial_student).text = equip.laboratoryMaterial
        view.findViewById<TextView>(R.id.pertenece_student).text = equip.memberChemicalDepartment.toString()
        view.findViewById<TextView>(R.id.q2_tv_course_student).text = equip.course
        view.findViewById<TextView>(R.id.q3_tv_professor_Student).text = equip.professor
        view.findViewById<TextView>(R.id.experiment_student).text = equip.experiment
        view.findViewById<TextView>(R.id.laboratory_student).text = equip.laboratory
        view.findViewById<TextView>(R.id.laboratoryEquipment_student).text = equip.laboratoryEquipment
        view.findViewById<TextView>(R.id.laboratoryMaterial_student).text = equip.laboratoryMaterial
        view.findViewById<Button>(R.id.button_student).setOnClickListener {
            view?.findNavController()?.navigate(R.id.action_detailPesStudentFragment_to_pesListStudentFragment_student)
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, APP_SCREEN_NAME)
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, APP_SCREEN_CLASS)
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, nameEquip)
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle)
    }
}
