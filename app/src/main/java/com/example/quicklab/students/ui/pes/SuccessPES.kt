package com.example.quicklab.students.ui.pes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.quicklab.R

class SuccessPES : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_success, container, false)
        view.findViewById<Button>(R.id.ok).setOnClickListener{
            view.findNavController().navigate(R.id.action_successPES_to_pesListStudentFragment_student)
        }
        return view
    }
}