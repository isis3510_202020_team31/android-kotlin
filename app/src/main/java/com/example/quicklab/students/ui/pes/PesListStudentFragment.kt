package com.example.quicklab.students.ui.pes

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.quicklab.R
import com.example.quicklab.UserSingleton
import com.example.quicklab.connectivity.PesRepository
import com.example.quicklab.worker.ui.BlogRecyclerAdapter
import com.example.quicklab.worker.ui.TopSpacingItemDecoration
import com.google.firebase.firestore.FirebaseFirestore


class PesListStudentFragment : Fragment() {
    private lateinit var blogAdapter: BlogRecyclerAdapter
    private lateinit var recycler_view_pes: RecyclerView
    private lateinit var sendAnother: Button
    private lateinit var frame_Layout_student_list_pes: FrameLayout
    private lateinit var textView_noInternet_student: TextView
    private lateinit var nested_scroll_view_student: NestedScrollView
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_pes_list_student, container, false)
        recycler_view_pes = root.findViewById(R.id.recycler_view_pes)
        sendAnother = root.findViewById(R.id.sendAnother)
        frame_Layout_student_list_pes = root.findViewById(R.id.frame_Layout_student_list_pes)
        textView_noInternet_student = root.findViewById(R.id.textView_noInternet_student)
        nested_scroll_view_student = root.findViewById(R.id.nested_scroll_view_student)
        val lab = UserSingleton.getInstance().userName
        if(lab!=null){
            PesRepository.retrievePesListFromFirebaseStudent(requireContext(),
                    lab!!,
                    ::initRecyclerView,
                    ::addDataSet,
                    ::onFailListener,
                    ::noInternetConnectionCallback)
        }


        sendAnother.setOnClickListener{
            view?.findNavController()?.navigate(R.id.action_pesListStudentFragment_student_to_nav_pes)
        }
        return root
    }

    private fun noInternetConnectionCallback(){
        this.textView_noInternet_student.visibility = View.VISIBLE
        this.frame_Layout_student_list_pes.visibility = View.VISIBLE
        this.nested_scroll_view_student.visibility =View.GONE
    }


    private fun addDataSet(content: List<Pes>?){
        //val data = DataSource.createDataSet()
        if(content!=null){
            blogAdapter.submitList(content)
        }

    }

    private fun onFailListener(exception: Exception){
        val duration = Toast.LENGTH_LONG
        val toast3 = Toast.makeText(context, "There was a problem" +exception.message, duration)
        toast3.show()
    }

    private fun initRecyclerView(){
        recycler_view_pes.apply {
            layoutManager = LinearLayoutManager(context)
            val topSpacingDecorator = TopSpacingItemDecoration(10)
            addItemDecoration(topSpacingDecorator)
            blogAdapter = BlogRecyclerAdapter()
            adapter = blogAdapter
        }
    }

}
