package com.example.quicklab.students.ui.properties

import android.content.ContentValues
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.example.quicklab.R
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL


class PropertiesFragment : Fragment() {

    internal class Weather : AsyncTask<String?, Void?, String?>() {
        override fun doInBackground(vararg address: String?): String? {
            val url = URL(address.get(0))
            address.get(0)
            val connection = url.openConnection() as HttpURLConnection
            try {
                connection.connect()
                val a = connection.inputStream
                val isr = InputStreamReader(a)
                var data = isr.read()
                var content = ""
                var ch: Char
                while (data != -1) {
                    ch = data.toChar()
                    content = content + ch
                    data = isr.read()
                }
                return content

            } catch (e: Exception){
                return null
            }

        }
    }


    private lateinit var firebaseAnalytics: FirebaseAnalytics
    val APP_SCREEN_CLASS = "PropertiesFragment"
    val APP_SCREEN_NAME = "Properties"



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_properties, container, false)
        val weather = Weather()
        var envVar: String = "https://api.openweathermap.org/data/2.5/weather?q=Bogota&appid=eb5a6908f877d6da175f09bd93833dd6"
        val content = weather.execute(envVar).get()
       if(content!=null){

           root.findViewById<FrameLayout>(R.id.frameLayout).setVisibility(View.GONE);

            val jsonObject = JSONObject(content)
            val weatherData: String = jsonObject.getString("weather")
            val mainTemperature: String = jsonObject.getString("main")
            val array: JSONArray = JSONArray(weatherData)
            var main = ""
            var temperature = ""
            var pressure = ""
            for(i in 0 until array.length() ){
                val weatherPart: JSONObject = array.getJSONObject(i);
                main = weatherPart.getString("main");

            }


            val mainPart = JSONObject(mainTemperature)
            temperature = mainPart.getString("temp")
            pressure = mainPart.getString("pressure")
            root.findViewById<TextView>(R.id.temperature)?.text = "Temperture: "+temperature+" K"
            root.findViewById<TextView>(R.id.pressure)?.text = "Pressure: "+pressure+" hPa"





            var spinner: Spinner = root.findViewById<Spinner>(R.id.spinner)
            var options = arrayListOf("Water", "Ethanol")

            spinner.adapter =  ArrayAdapter<String>(requireContext(),android.R.layout.simple_list_item_1,options)
            spinner.onItemSelectedListener = object : AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {
                override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    TODO("Not yet implemented")
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    TODO("Not yet implemented")
                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    var selection: String = options.get(p2)
                    val inputStream = resources.openRawResource(R.raw.cp_constants)
                    val inputStream_2 = resources.openRawResource(R.raw.antoine_constants)
                    val jsonAntoine = inputStream_2.bufferedReader().use(BufferedReader::readText)
                    val jsonProductsString = inputStream.bufferedReader().use(BufferedReader::readText)
                    var calculator: PropertiesCalculator = PropertiesCalculator(jsonProductsString, jsonAntoine)

                    val cp = calculator.calculateCp(selection, temperature.toDouble())
                    root.findViewById<TextView>(R.id.cp_text).text = cp.toString() +" J/mol K"
                    val psat = calculator.calculatePsat(selection, temperature.toDouble())
                    root.findViewById<TextView>(R.id.p_sat).text = psat.toString() + "mmHg"
                }

            }

        }
        else{
           root.findViewById<ConstraintLayout>(R.id.ll).setVisibility(View.GONE);
       }
        return root
    }

    override fun onResume() {
        super.onResume()


        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, APP_SCREEN_NAME)
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, APP_SCREEN_CLASS)
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle)


    }
}
