package com.example.quicklab

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.quicklab.model.UserStudentVO
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
class InitialMenuActivity : AppCompatActivity() {
    //en esta clase debe esta el usuario, que deberia ser una clase padre
    // se debe enviar el usuario al perfil
    //se debe cargar la info de firebase
    private lateinit var appBarConfiguration: AppBarConfiguration
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = intent
        var user = intent.getParcelableExtra<UserStudentVO>(Constants.EXTRA_MESSAGE_AUTH)
        Log.d("MENU", user.toString())
        setContentView(R.layout.activity_initial_menu)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        fab_main.setOnClickListener { view ->
            FirebaseAuth.getInstance().signOut()
            val homeActivity = Intent(this, AuthActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            UserSingleton.getInstance().lab = null;
            startActivity(homeActivity)
            finish()
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
                R.id.nav_qr_code,
                R.id.nav_equip_list,
                R.id.nav_slideshow,
                R.id.nav_pes,
                R.id.nav_profileFragment,
                R.id.nav_properties
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        val viewLay : View = navView.getHeaderView(0)
        val emailText : TextView = viewLay.findViewById<TextView>(R.id.textView_MenuHeader_Login)
        val nameText : TextView = viewLay.findViewById<TextView>(R.id.textView_MenuHeader_Name)
        emailText.text = user?.email
        nameText.text = user?.name
        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.nav_profileFragment) {
                Log.d("MENU", "listener funciona -------------")
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.initial_menu, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        @Nullable data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
    }
}


