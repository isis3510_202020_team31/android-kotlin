package com.example.quicklab.worker.ui

import android.os.Bundle
import android.os.Parcelable
import androidx.navigation.NavDirections
import com.example.quicklab.R
import java.io.Serializable
import java.lang.UnsupportedOperationException

class PesFragmentDirections private constructor() {
    private data class ActionNavPesListToPesDetailFragment(
            val Pes: com.example.quicklab.students.ui.pes.Pes,
            val role: String
    ) : NavDirections {
        override fun getActionId(): Int {
            if(role.equals("students")){
                return R.id.pes_list_to_detail_student
            }
            return R.id.action_pesWorkerFragment2_to_detailPesWorkerFragment
        }

        @Suppress("CAST_NEVER_SUCCEEDS")
        override fun getArguments(): Bundle {
            val result = Bundle()
            if (Parcelable::class.java.isAssignableFrom(Pes::class.java)) {
                result.putParcelable("Pes", this.Pes as Parcelable)
            } else if (Serializable::class.java.isAssignableFrom(Pes::class.java)) {
                result.putSerializable("Pes", this.Pes as Serializable)
            } else {
                throw UnsupportedOperationException(Pes::class.java.name +
                        " must implement Parcelable or Serializable or must be an Enum.")
            }
            return result
        }
    }

    companion object {
        fun actionNavEquipListToEquipmentDetailFragment(pes: com.example.quicklab.students.ui.pes.Pes, role: String): NavDirections =
                ActionNavPesListToPesDetailFragment(pes, role)
    }
}
