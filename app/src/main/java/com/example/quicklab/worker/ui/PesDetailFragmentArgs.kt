package com.example.quicklab.worker.ui

import android.os.Bundle
import android.os.Parcelable
import androidx.navigation.NavArgs
import com.example.quicklab.students.ui.pes.Pes
import java.io.Serializable
import java.lang.IllegalArgumentException
import java.lang.UnsupportedOperationException


data class PesDetailFragmentArgs(
        val Pes: Pes
) : NavArgs {
    @Suppress("CAST_NEVER_SUCCEEDS")
    fun toBundle(): Bundle {
        val result = Bundle()
        if (Parcelable::class.java.isAssignableFrom(com.example.quicklab.students.ui.pes.Pes::class.java)) {
            result.putParcelable("Pes", this.Pes as Parcelable)
        } else if (Serializable::class.java.isAssignableFrom(Pes::class.java)) {
            result.putSerializable("Pes", this.Pes as Serializable)
        } else {
            throw UnsupportedOperationException(Pes::class.java.name +
                    " must implement Parcelable or Serializable or must be an Enum.")
        }
        return result
    }

    companion object {
        @JvmStatic
        fun fromBundle(bundle: Bundle): PesDetailFragmentArgs {
            bundle.setClassLoader(PesDetailFragmentArgs::class.java.classLoader)
            val __Pes : Pes?
            if (bundle.containsKey("Pes")) {
                if (Parcelable::class.java.isAssignableFrom(Pes::class.java) ||
                        Serializable::class.java.isAssignableFrom(Pes::class.java)) {
                    __Pes = bundle.get("Pes") as Pes?
                } else {
                    throw UnsupportedOperationException(Pes::class.java.name +
                            " must implement Parcelable or Serializable or must be an Enum.")
                }
                if (__Pes == null) {
                    throw IllegalArgumentException("Argument \"Pes\" is marked as non-null but was passed a null value.")
                }
            } else {
                throw IllegalArgumentException("Required argument \"Pes\" is missing and does not have an android:defaultValue")
            }
            return PesDetailFragmentArgs(__Pes)
        }
    }
}
