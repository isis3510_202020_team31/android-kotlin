package com.example.quicklab.worker.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.quicklab.R
import com.example.quicklab.UserSingleton
import com.example.quicklab.connectivity.PesRepository
import com.example.quicklab.students.ui.pes.Pes
import kotlinx.android.synthetic.main.fragment_pes_worker.*

class PesWorkerFragment : Fragment() {

    private lateinit var blogAdapter: BlogRecyclerAdapter
    private lateinit var recycler_view_pes: RecyclerView
    private lateinit var frameLayout: FrameLayout
    private lateinit var scrollView: NestedScrollView
    private lateinit var textView_noInternet: TextView
    private lateinit var sendAnother: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_pes_worker, container, false)
        recycler_view_pes = root.findViewById(R.id.recycler_view_pes_worker)
        frameLayout =  root.findViewById(R.id.frame_Layout)
        scrollView = root.findViewById(R.id.scrollViewWorker)
        textView_noInternet = root.findViewById(R.id.textView_noInternet)
        val lab = UserSingleton.getInstance().lab
        if(lab!=null){
            PesRepository.retrievePesListFromFirebaseWorker(requireContext(),lab!!,::initRecyclerView,::addDataSet,::onFailListener,::noInternetConnectionCallback)
        }
        return root
    }

    private fun noInternetConnectionCallback(){
        this.textView_noInternet.visibility = View.VISIBLE
        this.frameLayout.visibility = View.VISIBLE
        this.scrollView.visibility =View.GONE

    }

    private fun onFailListener(exception: Exception){
        val duration = Toast.LENGTH_LONG
        val toast3 = Toast.makeText(context, "There was a problem" +exception.message, duration)
        toast3.show()
    }

    private fun addDataSet(content: List<Pes>?){
        if(content!=null){
            blogAdapter.submitList(content)
        }
    }

    private fun initRecyclerView(){
        requireActivity().frame_Layout.visibility =View.GONE
        textView_noInternet.visibility = View.GONE
        recycler_view_pes.apply {
            layoutManager = LinearLayoutManager(context)
            val topSpacingDecorator = TopSpacingItemDecoration(10)
            addItemDecoration(topSpacingDecorator)
            blogAdapter = BlogRecyclerAdapter()
            adapter = blogAdapter
        }
    }
}
