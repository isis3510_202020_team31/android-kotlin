package com.example.quicklab.worker.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.quicklab.R
import com.example.quicklab.UserSingleton
import com.example.quicklab.students.ui.equipment.EquipmentDetailFragmentArgs
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase

class DetailPesWorkerFragment : Fragment() {

    val args: PesDetailFragmentArgs by navArgs()
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    val APP_SCREEN_CLASS = "DetailPesWorkerFragment"
    val APP_SCREEN_NAME = "PesDetail"
    lateinit var nameEquip: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_detail_pes_worker, container, false)
        var equip = args.Pes
        nameEquip = equip.userName.toString()
        view.findViewById<TextView>(R.id.laboratoryMaterial_worker).text = equip.laboratoryMaterial
        view.findViewById<TextView>(R.id.pertenece_worker).text = equip.memberChemicalDepartment.toString()
        view.findViewById<TextView>(R.id.q2_tv_course_worker).text = equip.course
        view.findViewById<TextView>(R.id.q3_tv_professor_worker).text = equip.professor
        view.findViewById<TextView>(R.id.experiment_worker).text = equip.experiment
        view.findViewById<TextView>(R.id.laboratory_worker).text = equip.laboratory
        view.findViewById<TextView>(R.id.laboratoryEquipment_worker).text = equip.laboratoryEquipment
        view.findViewById<TextView>(R.id.laboratoryMaterial_worker).text = equip.laboratoryMaterial

        view.findViewById<Button>(R.id.button).setOnClickListener {
            view?.findNavController()?.navigate(R.id.action_detailPesWorkerFragment_to_pesWorkerFragment2)
        }

        return view
    }

    override fun onResume() {
        super.onResume()

        // After enough time has passed to make this screen view significant.
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, APP_SCREEN_NAME)
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, APP_SCREEN_CLASS)
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, nameEquip)
        //bundle.putString(MyAppAnalyticsConstants.Param.TOPIC, topic)
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle)

    }

}