package com.example.quicklab.worker.ui



import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.quicklab.R
import com.example.quicklab.UserSingleton
import kotlinx.android.synthetic.main.layout_blog_list_item.view.*
import com.example.quicklab.students.ui.pes.Pes
import kotlin.collections.ArrayList


class BlogRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{

    private val TAG: String = "AppDebug"

    private var items: List<Pes> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return BlogViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.layout_blog_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {

            is BlogViewHolder -> {
                holder.bind(items.get(position))
                holder.itemView.setOnClickListener {
                    val product = items[position]
                    var rol = "workers"
                    if(UserSingleton.getInstance().lab == null){
                        rol = "students"
                    }

                        val action = PesFragmentDirections.actionNavEquipListToEquipmentDetailFragment(product,rol)
                        holder.itemView.findNavController().navigate(action)

                }
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun submitList(blogList: List<Pes>){
        items = blogList
    }

    class BlogViewHolder
    constructor(
            itemView: View
    ): RecyclerView.ViewHolder(itemView){


        val blog_title = itemView.blog_title
        val blog_author = itemView.blog_author

        fun bind(pes: Pes){
            if(pes.status.equals("Waiting")){
                itemView.imagePes.setColorFilter(Color.parseColor("#fcc300"))
            }
            blog_title.setText(pes.userName)
            blog_author.setText(pes.status)
        }

    }

}
