package com.example.quicklab.worker.ui


data class Pes2(

        var userName: String?,

        var laboratoryMaterial: String?,

        var course: String?,

        var experiment: String?,

        var memberChemicalDepartment: Boolean?,

        var professor: String?,

        var laboratoryEquipment: String?,

        var laboratory: String

) {

    override fun toString(): String {
        return "BlogPost(title='$userName', image='$course', username='$experiment')"
    }
}