package com.example.quicklab.worker.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.quicklab.R
import com.example.quicklab.Room.UserRepository


class HomeFragmentWorker
    : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_profile_worker, container, false)
        root.findViewById<TextView>(R.id.profile_username_worker).text = UserRepository.worker.name
        root.findViewById<TextView>(R.id.profile_email_worker).text = UserRepository.worker.name

        return root
    }
}
