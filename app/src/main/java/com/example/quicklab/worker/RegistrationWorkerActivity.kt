package com.example.quicklab.worker

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import android.widget.Toast
import com.example.quicklab.Constants
import com.example.quicklab.InitialMenuActivity2
import com.example.quicklab.R
import com.example.quicklab.Room.UserRepository
import com.example.quicklab.UserSingleton
import com.example.quicklab.connectivity.NetworkMonitor
import com.example.quicklab.model.RolUser
import com.example.quicklab.model.UserWorkerWO
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_registration_worker.confirm_password_t
import kotlinx.android.synthetic.main.activity_registration_worker.email
import kotlinx.android.synthetic.main.activity_registration_worker.name
import kotlinx.android.synthetic.main.activity_registration_worker.password_t


class RegistrationWorkerActivity : AppCompatActivity() {
    private lateinit var spinner_lab_pes: Spinner
    var student : UserWorkerWO = UserWorkerWO()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(NetworkMonitor.isOnline(this)){
            setContentView(R.layout.activity_registration_worker)
            setup()
        }
        else{
            setContentView(R.layout.no_internet)
        }


    }

    private fun setup() {
        spinner_lab_pes = findViewById(R.id.spinner_lab_pes)
        var options = arrayOf("ML-037","ML-206","ML-305","ML-414/416","ML-418")
        spinner_lab_pes.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, options)
        findViewById<Button>(R.id.register_worker_button).setOnClickListener{
            if(!name.text.isNullOrEmpty() &&
                    !password_t.text.isNullOrEmpty() &&
                    !confirm_password_t.text.isNullOrEmpty() &&
                    !email.text.isNullOrEmpty())
                if( password_t.text.toString() ==  confirm_password_t.text.toString()){
                    val db = FirebaseAuth.getInstance()
                    val e = email.text.toString()
                    val p = password_t.text.toString()
                    db.createUserWithEmailAndPassword(e, p).addOnCompleteListener{
                        val db = FirebaseFirestore.getInstance()
                        student = UserWorkerWO(name.text.toString(), email.text.toString(), spinner_lab_pes.selectedItem.toString())
                        val rolUser: RolUser = RolUser(email.text.toString(),"workers")
                        if (it.isSuccessful){
                            UserSingleton.getInstance().userName = e
                            db.collection("workers").document().set(student)
                            db.collection("users").document().set(rolUser)
                            showMenu()
                        } else {
                            showError()
                        }
                    }.addOnFailureListener { exception ->
                        val duration = Toast.LENGTH_LONG
                        val toast3 = Toast.makeText(this, "There was a problem with databse connection. Please try again later.", duration)
                        toast3.show()
                    }
                }
        }
    }
    fun showError() {
        val duration = Toast.LENGTH_LONG
        val toast3 = Toast.makeText(this, "Something went wrong, try again later", duration)
        toast3.show()
    }

    fun showMenu() {
        val profileIntent = Intent(this, InitialMenuActivity2::class.java)
        profileIntent.putExtra(Constants.EXTRA_MESSAGE_AUTH, student)

        UserRepository.worker = student
        UserRepository.rol = UserRepository.WORKER
        startActivity(profileIntent)
    }
}
