package com.example.quicklab.worker.ui.pes

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.quicklab.R
import com.example.quicklab.students.ui.pes.Pes

public class PesItemAdapter : RecyclerView.Adapter<PesItemAdapter.ViewHolder>{
    val listPes : List<Pes>
    val inflater:LayoutInflater
    val context: Context
    constructor(listPes : List<Pes>,context: Context) {
        this.listPes = listPes;
        this.inflater = LayoutInflater.from(context)
        this.context = context
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = inflater.inflate(R.layout.pes_element_view, null)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listPes.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(listPes.get(position))
    }

    public class ViewHolder : RecyclerView.ViewHolder {
        val iconImage: ImageView
        val name: TextView


        constructor(itemView: View) : super(itemView) {
            iconImage = itemView.findViewById(R.id.imagePes)
            name = itemView.findViewById(R.id.nameTextView)
        }

        fun bindData(item: Pes){
            name.setText(item.userName)
        }


    }


}