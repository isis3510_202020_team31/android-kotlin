package com.example.quicklab.Room

import android.util.Log
import androidx.annotation.WorkerThread
import com.example.quicklab.Room.dao.EquipmentDao
import com.example.quicklab.model.EquipmentVO
import kotlinx.coroutines.flow.Flow

class EquipmentRepository(private val equipDao: EquipmentDao) {

    // Room executes all queries on a separate thread.
    // Observed Flow will notify the observer when the data has changed.
    //val allEquipments: Flow<List<EquipmentVO>> = equipDao.getEquipments()

    // By default Room runs suspend queries off the main thread, therefore, we don't need to
    // implement anything else to ensure we're not doing long running database work
    // off the main thread.
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(equip: EquipmentVO) {
        equipDao.insert(equip)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertAll(equipments: List<EquipmentVO>) {
        equipDao.insertAll(equipments)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun updateAll(equipments: List<EquipmentVO>) {
        equipDao.updateAll(equipments)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun getAll() : List<EquipmentVO> {
        val nn = equipDao.getEquipments()
        Log.d("BASE DE DATOS", nn.toString())
        return nn
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun get(nameE: String) : List<EquipmentVO> {
        return equipDao.getEquipment(nameE)
    }
}
