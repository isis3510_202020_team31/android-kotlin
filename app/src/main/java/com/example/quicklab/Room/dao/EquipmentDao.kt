package com.example.quicklab.Room.dao

import androidx.room.*
import com.example.quicklab.model.EquipmentVO
import kotlinx.coroutines.flow.Flow

@Dao
interface EquipmentDao {

    @Query("SELECT * FROM equipment")
    suspend fun getEquipments(): List<EquipmentVO>

    @Query("SELECT * FROM equipment WHERE name LIKE :nameE")
    suspend fun getEquipment(nameE: String): List<EquipmentVO>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(equipment: EquipmentVO)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(equipments: List<EquipmentVO>)

    @Query("DELETE FROM equipment")
    suspend fun deleteAll()

    @Update
    suspend fun update(equipment: EquipmentVO)

    @Update
    suspend fun updateAll(equipments: List<EquipmentVO>)
}
