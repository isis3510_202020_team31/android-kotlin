package com.example.quicklab

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.quicklab.model.UserStudentVO
import com.example.quicklab.model.UserWorkerWO
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth


class InitialMenuActivity2 : AppCompatActivity() {
    private lateinit var appBarConfiguration: AppBarConfiguration
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_worker)
        var user = intent.getParcelableExtra<UserWorkerWO>(Constants.EXTRA_MESSAGE_AUTH)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            FirebaseAuth.getInstance().signOut()
            val homeActivity = Intent(this, AuthActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            UserSingleton.getInstance().lab = null;
            startActivity(homeActivity)
            finish()
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout_worker)
        val navView: NavigationView = findViewById(R.id.nav_view_worker)
        val navController = findNavController(R.id.nav_host_fragment_worker)
        appBarConfiguration = AppBarConfiguration(setOf(
                R.id.nav_home,
            R.id.pesWorkerFragment2,
            R.id.nav_profile_worker

        ), drawerLayout)

        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        val viewLay : View = navView.getHeaderView(0)
        val emailText : TextView = viewLay.findViewById<TextView>(R.id.textView_MenuHeader_Login_Worker)
        val nameText : TextView = viewLay.findViewById<TextView>(R.id.textView_MenuHeader_Name_Worker)
        emailText.text = user?.email
        nameText.text = user?.name

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main_menu_worker, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_worker)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

}
