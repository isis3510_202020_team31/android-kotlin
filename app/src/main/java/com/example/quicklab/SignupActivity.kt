package com.example.quicklab

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.findNavController
import com.example.quicklab.Room.UserRepository
import com.example.quicklab.connectivity.NetworkMonitor
import com.example.quicklab.model.RolUser
import com.example.quicklab.model.UserStudentVO
import com.example.quicklab.students.ui.pes.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_signup.*

class SignupActivity : AppCompatActivity() {
    var student : UserStudentVO = UserStudentVO()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(NetworkMonitor.isOnline(this)){
            setContentView(R.layout.activity_signup)
            setUp()
        }
        else{
            setContentView(R.layout.no_internet)
        }

    }
    fun setUp() {
        findViewById<Button>(R.id.register).setOnClickListener{
            if(!name.text.isNullOrEmpty()&&
                    !code_t.text.isNullOrEmpty() &&
                    !password_t.text.isNullOrEmpty() &&
                    !confirm_password_t.text.isNullOrEmpty() &&
                    !email.text.isNullOrEmpty())
            if( password_t.text.toString() ==  confirm_password_t.text.toString()){
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email.text.toString(), password_t.text.toString()).addOnCompleteListener{
                    val db = FirebaseFirestore.getInstance()
                    student = UserStudentVO(name.text.toString(), email.text.toString() , code_t.text.toString().toInt())
                    val rolUser: RolUser = RolUser(email.text.toString(),"students")
                    if (it.isSuccessful){
                        db.collection("students").document().set(student)
                        db.collection("users").document().set(rolUser)
                        showMenu()
                    } else {
                        showError()
                    }
                }.addOnFailureListener { exception ->
                    val duration = Toast.LENGTH_LONG
                    val toast3 = Toast.makeText(this, "There was a problem uploading your request to the database. Please try again later.", duration)
                    toast3.show()
                }
            }
        }
    }

    fun showError() {
        val duration = Toast.LENGTH_LONG
        val toast3 = Toast.makeText(this, "Something went wrong, try again later", duration)
        toast3.show()
    }

    fun showMenu() {

        val profileIntent = Intent(this,InitialMenuActivity::class.java)
        profileIntent.putExtra(Constants.EXTRA_MESSAGE_AUTH, student)
        UserRepository.user = student
        UserRepository.rol = UserRepository.STUDENT

        UserSingleton.getInstance().userName = student.email
        startActivity(profileIntent)
    }
}
