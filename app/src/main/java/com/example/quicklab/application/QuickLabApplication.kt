package com.example.quicklab.application

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.example.quicklab.Room.EquipmentRepository
import com.example.quicklab.Room.rooms.EquipmentRoomDB

class QuickLabApplication : Application() {



    companion object {

        lateinit var instance: QuickLabApplication
            private set

        val database by lazy { EquipmentRoomDB.getDatabase(instance) }
        val repository by lazy { EquipmentRepository(database.equipDao()) }

    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

}
