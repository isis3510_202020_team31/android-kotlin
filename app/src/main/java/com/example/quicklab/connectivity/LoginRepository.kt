package com.example.quicklab.connectivity

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.temp.*

class LoginRepository {
    companion object {
        fun isOnline(context: Context): Boolean {
            val connectivityManager =
                    context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (connectivityManager != null) {
                val capabilities =
                        connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                        return true
                    }
                }
            }
            return false
        }

        fun login(context: Context, email: String, password: String, sucessCallback: (response: Boolean, internetConnection: Boolean) -> Unit) {
            if (isOnline(context)) {
                FirebaseAuth.getInstance()
                        .signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener {
                            if (it.isSuccessful) {
                                sucessCallback(true, true)
                            }
                            else{
                                sucessCallback(false, true)
                            }
                        }
            }
            else{
                val preferences: SharedPreferences = context.getSharedPreferences("users", Context.MODE_PRIVATE)
                val pass: String? = preferences.getString(email, "")
                if(pass.equals(password)){
                    sucessCallback(true,false)
                }
                else{
                    sucessCallback(false, false)
                }
            }


        }
    }
}


