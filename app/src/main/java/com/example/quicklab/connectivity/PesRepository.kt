package com.example.quicklab.connectivity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import com.example.quicklab.FeatureUse
import com.example.quicklab.InitialMenuActivity
import com.example.quicklab.InitialMenuActivity2
import com.example.quicklab.UserSingleton
import com.example.quicklab.students.ui.pes.CurrentUser
import com.example.quicklab.students.ui.pes.Pes
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.temp.*

class PesRepository {

    companion object {
        fun isOnline(context: Context): Boolean {
            val connectivityManager =
                    context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (connectivityManager != null) {
                val capabilities =
                        connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                        return true
                    }
                }
            }
            return false
        }

        fun getPes(valueLaboratory: Boolean, sucessCallback: (response: List<Pes>) -> Unit, failCallback: (exception: Exception) -> Unit) {
            val db = FirebaseFirestore.getInstance()
            val pes: List<Pes>
            db.collection("users")
                    .whereEqualTo("laboratory", valueLaboratory)
                    .get()
                    .addOnSuccessListener { documents ->
                        val pes = documents.toObjects(Pes::class.java)
                        sucessCallback(pes)
                    }
                    .addOnFailureListener { exception ->
                        failCallback(exception)
                    }
        }


        fun sendPes(context: Context, activity: FragmentActivity, pes: Pes, sucessCallback: (response: Boolean) -> Unit) {
            var feature: FeatureUse = FeatureUse("pes")
            val db = FirebaseFirestore.getInstance()
            db.collection("features").document().set(feature)
            if (isOnline(context)) {
                db.collection("pes").document().set(pes).addOnSuccessListener {
                    cleanCache(context)
                    sucessCallback(true)
                }.addOnFailureListener {
                    savePesInCache(context, pes)
                    sucessCallback(false)
                }
            } else {
                savePesInCache(context, pes)
                sucessCallback(false)
            }
        }

        fun savePesInCache(context: Context, pes: Pes) {
            val preferences: SharedPreferences = context.getSharedPreferences("pes", Context.MODE_PRIVATE)
            val editor: SharedPreferences.Editor = preferences.edit()
            editor.putBoolean("memberChemicalDepartment", pes.memberChemicalDepartment)
            editor.putBoolean("pes", true)
            editor.putString("professor", pes.professor.toString())
            editor.putString("course", pes.course.toString())
            editor.putString("experiment", pes.experiment.toString())
            editor.putString("laboratoryEquipment", pes.laboratoryEquipment.toString())
            editor.putString("laboratoryMaterial", pes.laboratoryMaterial.toString())
            editor.putString("userName", pes.userName.toString())
            editor.putString("date", pes.date.toString())
            editor.apply()
        }

        fun cleanCache(context: Context) {
            val preferences: SharedPreferences = context.getSharedPreferences("pes", Context.MODE_PRIVATE)
            val editor: SharedPreferences.Editor = preferences.edit()
            editor.clear().commit()
        }

        public fun retrievePesListFromFirebaseWorker(context: Context,
                                                     lab: String,
                                                     initRecyclerView: () -> Unit,
                                                     addDataSet: (content: List<Pes>?) -> Unit,
                                                     onFailListener: (exception: Exception) -> Unit,
                                                     noInternetConnectionCallback: () -> Unit
        ) {
            if (isOnline(context)) {
                FirebaseFirestore.getInstance().collection("pes")
                        .whereEqualTo("laboratory", lab)
                        .get()
                        .addOnSuccessListener { documents ->
                            initRecyclerView()
                            addDataSet(documents.toObjects(Pes::class.java))
                        }
                        .addOnFailureListener { exception ->
                            onFailListener(exception)
                        }
            } else {
                noInternetConnectionCallback()
            }
        }


        public fun retrievePesListFromFirebaseStudent(context: Context,
                                                      username: String,
                                                      initRecyclerView: () -> Unit,
                                                      addDataSet: (content: List<Pes>?) -> Unit,
                                                      onFailListener: (exception: Exception) -> Unit,
                                                      noInternetConnectionCallback: () -> Unit
        ) {
            if (isOnline(context)) {
                val db = FirebaseFirestore.getInstance()
                db.collection("pes")
                        .whereEqualTo("userName", username)
                        .get()
                        .addOnSuccessListener { documents ->

                            initRecyclerView()
                            addDataSet(documents.toObjects(Pes::class.java))
                        }
                        .addOnFailureListener { exception ->
                            onFailListener(exception)
                        }
            } else {
                noInternetConnectionCallback()
            }

        }

    }
}