package com.example.quicklab

class UserSingleton {
    var userName: String ? = null
    var lab: String ? = null
    companion object{
        private var instance : UserSingleton? = null
        fun getInstance(): UserSingleton{
            if(instance==null){
                instance = UserSingleton();
            }
            return instance as UserSingleton
        }
    }
}