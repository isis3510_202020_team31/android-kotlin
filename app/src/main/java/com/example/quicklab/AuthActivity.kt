package com.example.quicklab

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.quicklab.connectivity.LoginRepository
import com.example.quicklab.students.ui.pes.CurrentUser
import com.example.quicklab.worker.RegistrationWorkerActivity
import com.google.firebase.firestore.FirebaseFirestore
import androidx.lifecycle.lifecycleScope
import com.example.quicklab.Room.UserRepository
import com.example.quicklab.model.UserStudentVO
import com.example.quicklab.model.UserWorkerWO
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.temp.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class AuthActivity() : AppCompatActivity() {
    lateinit var login : String
    lateinit var rol : String
    var successLogin : Boolean = false
    lateinit var user_student : UserStudentVO
    lateinit var user_worker: UserWorkerWO
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.temp)
    }
    override fun onResume() {
        super.onResume()
        setup()
    }

    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {

        return super.onCreateView(name, context, attrs)
    }

    private fun getUserFromCache(){
        val preferences: SharedPreferences = applicationContext.getSharedPreferences("currentUser", Context.MODE_PRIVATE)
        val usuario: String? = preferences.getString("user", "")
        val password: String? = preferences.getString("password", "")
        user_edit_text.setText(usuario.toString())
        password_edit_text.setText(password.toString())

    }


    private fun updateUser(){
        val preferences: SharedPreferences = applicationContext.getSharedPreferences("currentUser", Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.putString("user",user_edit_text.text.toString())
        editor.putString("password",password_edit_text.text.toString())
        editor.commit()
    }

    private fun setup() {
        getUserFromCache()
        title = getString(R.string.authentication)
        next_button.setOnClickListener {
            if (!user_edit_text.text.isNullOrEmpty() && !password_edit_text.text.isNullOrEmpty()) {
                if(remember_me_checkbox.isChecked){
                    val preferences: SharedPreferences = applicationContext.getSharedPreferences("users", Context.MODE_PRIVATE)
                    val editor: SharedPreferences.Editor = preferences.edit()
                    editor.putString(user_edit_text.text.toString(), password_edit_text.text.toString())
                    editor.commit()
                    updateUser()
                }
                LoginRepository.login(applicationContext, user_edit_text.text.toString(), password_edit_text.text.toString(), ::callBackLogin)
            }
        }
        findViewById<TextView>(R.id.signup_text_student).setOnClickListener {
            val signUp = Intent(this, SignupActivity::class.java)
            startActivity(signUp)
        }
        findViewById<TextView>(R.id.signup_text_worker).setOnClickListener {
            val signUpWorker = Intent(this, RegistrationWorkerActivity::class.java)
            startActivity(signUpWorker)
        }
    }

    private fun callBackLogin(response: Boolean, internetConnection: Boolean){
        if(response && !internetConnection){
            val duration = Toast.LENGTH_LONG
            val toast3 = Toast.makeText(this, "The functionalities without internet connection are limited", duration)
            toast3.show()
            UserSingleton.getInstance().userName = user_edit_text.text.toString()
            login = user_edit_text.text.toString()
            successLogin = true
        }
        else if(response && internetConnection){
            UserSingleton.getInstance().userName = user_edit_text.text.toString()
            login = user_edit_text.text.toString()
            successLogin = true
        }
        else{
            showAlert()
        }
        if (successLogin){
            val db = FirebaseFirestore.getInstance()
            db.collection("users")
                    .whereEqualTo("email",  user_edit_text.text.toString())
                    .get()
                    .addOnSuccessListener { documents ->
                        val users = documents.toObjects(CurrentUser::class.java)
                        val user = users.get(0)
                        this.login = user.email
                        if(user.rol.equals("students")){
                            succesLogin("students")
                        }
                        else{
                            succesLogin("workers")
                        }
                    }
                    .addOnFailureListener { exception ->
                        val duration = Toast.LENGTH_LONG
                        val toast3 = Toast.makeText(this, "There was a problem uploading your request to the database. Please try again later.", duration)
                        toast3.show()
                    }
        }
    }

    private fun succesLogin(rol: String){
        lifecycleScope.launchWhenResumed {
            if (successLogin){
                loadUser(login, rol)
                showProfile(rol)
            }
        }
    }

    private fun showAlert() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Wrong credentials")
        builder.setMessage("The user or the password is wrong")
        val dialog: AlertDialog = builder.create();
        dialog.show()
    }

    private fun showProfile(rol: String) {
        if(rol.equals("students")){
            val profileIntent = Intent(this, InitialMenuActivity::class.java)
            profileIntent.putExtra(Constants.EXTRA_MESSAGE_AUTH, user_student)
            UserRepository.user = user_student
            UserRepository.rol = UserRepository.STUDENT
            startActivity(profileIntent)
        }
        else{
            UserSingleton.getInstance().userName = user_worker.email
            UserSingleton.getInstance().lab = user_worker.laboratory
            val profileIntent = Intent(this, InitialMenuActivity2::class.java)
            profileIntent.putExtra(Constants.EXTRA_MESSAGE_AUTH, user_worker)

            UserRepository.worker = user_worker
            UserRepository.rol = UserRepository.WORKER
            startActivity(profileIntent)
        }

        finish()
    }


    suspend fun loadUser(userLogin : String, rol : String)
            = withContext(Dispatchers.IO){
        var qS = getDataFromFireStore(userLogin, rol)
        if(qS != null){
            if(rol.equals("students")){
                user_student = giveStudentObjectFromQuery(qS)
            }
            else{
                user_worker = giveWorkerObjectFromQuery(qS)
            }
        }
    }


    suspend fun getDataFromFireStore(value : String, collection: String) : QuerySnapshot?
            = withContext(Dispatchers.IO){
        return@withContext try{
            val data = FirebaseFirestore.getInstance()
                    .collection(collection)
                    .whereEqualTo("email", value)
                    .get()
                    .await()
            data
        }catch (e : Exception){
            null
        }
    }
    fun giveStudentObjectFromQuery(qS : QuerySnapshot): UserStudentVO {
        var productList: MutableList<UserStudentVO> = mutableListOf<UserStudentVO>()
        val docs = qS.documents
        for (doc in docs) {
            doc.toObject(UserStudentVO::class.java)?.let {
                productList.add(it)
            }
        }
        return productList.get(0)
    }
    fun giveWorkerObjectFromQuery(qS : QuerySnapshot): UserWorkerWO {
        var productList: MutableList<UserWorkerWO> = mutableListOf<UserWorkerWO>()
        val docs = qS.documents
        for (doc in docs) {
            doc.toObject(UserWorkerWO::class.java)?.let {
                productList.add(it)
            }
        }
        return productList.get(0)
    }

}
